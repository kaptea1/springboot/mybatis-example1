package com.kp.springbootmybatis;

import com.kp.springbootmybatis.domain.Employee;
import com.kp.springbootmybatis.mapper.EmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeTests {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void getEmployeesTest1() {
        List<Employee> employees = employeeMapper.getEmployees();
        assertNotNull(employees);
        assertTrue(!employees.isEmpty());
    }

    @Test
    public void findEmployeeById1() {
        Employee employee = employeeMapper.getEmployee(1);
        assertNotNull(employee);
    }

    @Test
    public void createEmployee1() {
        Employee employee = new Employee("Kiran Kumar", "kiran@gmail.com", "CSE");
        employeeMapper.saveEmployee(employee);
        Employee createdEmployee = employeeMapper.getEmployee(7);
        assertEquals(employee.getFullName(), createdEmployee.getFullName());
        assertEquals(employee.getUserName(), createdEmployee.getUserName());
    }
}