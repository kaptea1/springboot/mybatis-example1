package com.kp.springbootmybatis.exception;

public class EmployeeNotExist extends RuntimeException {

    public EmployeeNotExist(String customMessage) {
        super(customMessage);
    }
}