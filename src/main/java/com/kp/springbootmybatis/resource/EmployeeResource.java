package com.kp.springbootmybatis.resource;

import com.kp.springbootmybatis.domain.Employee;
import com.kp.springbootmybatis.exception.EmployeeNotExist;
import com.kp.springbootmybatis.service.EmployeeService;
import jdk.nashorn.internal.runtime.logging.Logger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
public class EmployeeResource {

    private final EmployeeService employeeService;

    public EmployeeResource(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    //Create employee
    @PostMapping("/employee")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        Integer persistedObject = employeeService.createNewEmployee(employee);
        if(persistedObject != null){
            log.info("Employee " + employee.getFullName() + " created successfully");
        }
        return new ResponseEntity<>(employee, HttpStatus.CREATED);
    }

    //Get the list of employees
    @GetMapping("/employee")
    public ResponseEntity<List<Employee>> getEmployees() {
        List<Employee> objects = employeeService.getEmployees();
        if(objects.isEmpty()){
            throw new EmployeeNotExist("Employees not found");
        }
        log.info("Employees fetched successfully");
        return new ResponseEntity<>(objects, HttpStatus.OK);
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable int id) {
        Employee employee = employeeService.getEmployee(id);
        log.info("Employee fetched successfully");
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }
    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable int id) {
        employeeService.deleteEmployee(id);
        log.info("Employees deleted successfully");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}