package com.kp.springbootmybatis.service;

import com.kp.springbootmybatis.domain.Employee;
import com.kp.springbootmybatis.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EmployeeService {
    List<Employee> getEmployees();
    Integer createNewEmployee(Employee employee);

    Employee getEmployee(int id);
    void updateEmployee(Employee employee);
    void deleteEmployee(int id);
}