package com.kp.springbootmybatis.service;

import com.kp.springbootmybatis.domain.Employee;
import com.kp.springbootmybatis.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EmployeeDao implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Transactional(readOnly = true)
    public List<Employee> getEmployees() {
        return employeeMapper.getEmployees();
    }

    @Override
    public Integer createNewEmployee(Employee employee) {
        return employeeMapper.saveEmployee(employee);
    }

    @Override
    public Employee getEmployee(int id) {
        return employeeMapper.getEmployee(id);
    }

    @Override
    public void updateEmployee(Employee employee) {
        employeeMapper.saveEmployee(employee);
    }

    @Override
    public void deleteEmployee(int id) {
        employeeMapper.deleteEmployeeById(id);
    }
}